package io.phaelmoita.spark.twitter

import io.phaelmoita.spark.twitter.utils.HashTagsCombination
import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.functions.{col, size}
import scopt.OptionParser

object TwitterHashTagsCombination {

  var debugResult:Array[(String, String)] = _

  case class Params(
                     var inputPath: String = "./temp/data/output/*.json",
                     var debug: Boolean = true
                   )

  lazy val parser = new OptionParser[Params]("TwitterStreaming: params parse to execute stream.") {
    head("ExecuteQuery: params parse to execute query.")
    opt[String]("input-path")
      .text("server host stream")
      .action((x, c) => c.copy(inputPath = x))
    opt[Boolean]("debug")
      .text("if is debug and just show tweets")
      .action((x, c) => c.copy(debug = x))
    note(
      """
        |
      """.stripMargin)
  }

  def main(args: Array[String]): Unit = {
    val defaultParams = Params()

    parser.parse(args, defaultParams).map { p =>
      run(p)
    } getOrElse {
      System.exit(1)
    }
  }

  def run(params: Params): Unit = {

    var conf:SparkConf = new SparkConf()

    if (params.debug) {
      conf = new SparkConf()
        .setMaster("local")
        .setAppName("twitter-stream-debug")

    }

    val sparkSession:SparkSession = SparkSession
      .builder
      .config(conf)
      .getOrCreate()


    import sparkSession.implicits._

    val df = sparkSession
      .read
      .json(params.inputPath)
      .where(size(col("hashtags")) > 1)
      .flatMap(t => {
        //this map sort the hashtags and return all combinations then join in the same list
        HashTagsCombination
          .getStringsHashTagCombinations(t.getAs[List[String]]("hashtags").toArray)
          .map(h => {
            (t.getAs[String]("timestamp"), h)
          })
      })

    df.foreach(l => {
      println(s"""${l._1}, ${l._2}""")
    })

    if (params.debug) {
      debugResult = df.collect()
    }

    sparkSession.stop()
  }

}
