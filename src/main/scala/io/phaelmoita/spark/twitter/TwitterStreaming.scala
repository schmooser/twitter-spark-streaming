package io.phaelmoita.spark.twitter

import org.apache.spark.SparkConf
import org.apache.spark.sql.{SparkSession}
import org.apache.spark.sql.streaming.Trigger
import scopt.OptionParser

  object TwitterStreaming {

  case class Params(
                     var hostStream: String = "localhost",
                     var portStream: String = "9009",
                     var checkpointLocation: String = "./temp/data/checkpoint/",
                     var outputPath: String = "./temp/data/output/",
                     var tz: String = "UTC",
                     var debug:Boolean = true
                   )

  lazy val parser = new OptionParser[Params]("TwitterStreaming: params parse to execute stream.") {
    head("ExecuteQuery: params parse to execute query.")
    opt[String]("host-stream")
      .text("server host stream")
      .action((x, c) => c.copy(hostStream = x))
    opt[String]("port-stream")
      .text("port host stream")
      .action((x, c) => c.copy(portStream = x))
    opt[String]("checkpoint")
      .text("spark stream checkpoint")
      .action((x, c) => c.copy(checkpointLocation = x))
    opt[String]("output-path")
      .text("path of output stream")
      .action((x, c) => c.copy(outputPath = x))
    opt[String]("timezone")
      .text("timezone to log tweets")
      .action((x, c) => c.copy(tz = x))

    opt[Boolean]("debug")
      .text("if is debug and just show tweets")
      .action((x, c) => c.copy(debug = x))
    note(
      """
        |
      """.stripMargin)
  }

  def main(args: Array[String]): Unit = {
    val defaultParams = Params()

    parser.parse(args, defaultParams).map { p =>
      run(p)
    } getOrElse {
      System.exit(1)
    }
  }

  def run(params: Params): Unit = {

    var conf:SparkConf = new SparkConf()

    if (params.debug) {
      conf = new SparkConf()
        .setMaster("local")
        .setAppName("twitter-stream-debug")

    }

    val sparkSession:SparkSession = SparkSession
      .builder
      .config(conf)
      .getOrCreate()

    val socketDF = sparkSession.readStream
      .format("socket")
      .option("host", params.hostStream)
      .option("port", params.portStream)
      .option("sep", "\t")
      .load()

    //socketDF.writeStream.format("console").start().awaitTermination()

    import sparkSession.implicits._

    socketDF
      .map(l => {
        //this map parse all hashtags with regex and return an array
        val dt = l.get(0).toString.split("\t")
        val hashRegex = "\\B(\\#[a-zA-Z]+\\b)(?!;)".r
        val hashTags = (hashRegex findAllIn dt(2)).toArray

        (dt(0), dt(1), dt(2), hashTags)
      })
      .withColumnRenamed("_1", "id")
      .withColumnRenamed("_2", "timestamp")
      .withColumnRenamed("_3", "tweet")
      .withColumnRenamed("_4", "hashtags")
      .writeStream
      .format("json")
      .option("format", "append")
      .option("checkpointLocation", params.checkpointLocation)
      .option("path", params.outputPath)
      .outputMode("append")
      .trigger(Trigger.ProcessingTime(10000))
      .start()
      .awaitTermination()

  }

}
