package io.phaelmoita.test.spark.twitter

import io.phaelmoita.spark.twitter.utils.HashTagsCombination
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FlatSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class HashTagsCombinationTest extends FlatSpec with BeforeAndAfter with BeforeAndAfterAll with Matchers  {

  override def beforeAll() = {}

  "the combination size" should "be 4" in {
    val combinations = HashTagsCombination.getHashTagCombinations(Array[String]("#HBO", "#HBOBR", "#HisDarkMaterials"))

    combinations.size should equal(4)
  }

  "the last combination" should "be #de #oliveira #raphael #silva" in {
    val combinations = HashTagsCombination.getStringsHashTagCombinations(Array[String]("#raphael", "#silva", "#de", "#oliveira"))

    combinations.last should equal("#de #oliveira #raphael #silva")
  }

}
